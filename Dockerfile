FROM python:3

WORKDIR /urs/src/app

COPY .  /urs/src/app

RUN pip install -r requirements.txt

EXPOSE 5000

CMD [ "python", "app.py" ]